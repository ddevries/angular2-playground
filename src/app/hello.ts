import {Component} from '@angular/core';

@Component({
  selector: 'apo',
  template: require('./hello.html')
})
export class Hello {
  public greet: string;

  constructor() {
    this.greet = 'Hello World!';
  }
}

