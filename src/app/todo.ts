import {Component} from '@angular/core';

@Component({
  selector: 'App',
  template: require('./todo.html')
})
export class Todo {
  public greet: string;

  constructor() {
    this.greet = 'Hello World!';
  }
}
